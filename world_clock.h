#ifndef __WORLD_CLOCK_H__
#define __WORLD_CLOCK_H__

#include<string>
#include<iostream>
#include<fstream>
#include<cstdlib>
#include<map>

class WorldClock {

 public:

  WorldClock() {
	  time_difference_=0;
  }

  WorldClock(int hour, int minute, int second) {
	  hour_=hour;
	  minute_=minute;
	  second_=second;
	  time_difference_=0;
  }

  void Tick(int seconds = 1){
	  hour_+=seconds/3600;
	  while(hour_<0){hour_+=24;}
	  while(hour_>=24){hour_-=24;}
	  seconds=seconds%3600;
	  minute_+=seconds/60;
	  while(minute_>=60){minute_-=60;hour_++;}
	  while(minute_<0){minute_+=60;hour_--;}
	  seconds=seconds%60;
	  second_+=seconds%60;
	  while(second_>=60){second_-=60;minute_++;}
	  while(second_<0){second_+=60;minute_--;}
  }

  // 잘못된 값 입력시 false 리턴하고 원래 시간은 바뀌지 않음.

  bool SetTime(int hour, int minute, int second){
	  if(hour<0||hour>=24||minute<0||minute>=60||second<0||second>=60) return false;
	  hour_=hour;
	  minute_=minute;
	  second_=second;
	  return true;
  }


  static bool LoadTimezoneFromFile(const std::string& file_path){
	 std::ifstream fp;
	 fp.open(file_path.c_str());
	 std::string str;
	 int num;
	 while(!fp.eof()) {
		fp >> str >> num;
		timezone_[str]=num;
	 }
	 fp.close();
	 return true;
   }

 static void SaveTimezoneToFile(const std::string& file_path){
	 std::ofstream fp;
	 fp.open(file_path.c_str());
	 for(std::map<std::string, int>::iterator it=timezone_.begin(); it!=timezone_.end(); it++) {
		 fp<<it->first<<" "<<it->second<<std::endl;
	 }
	 fp.close();
 }

 static void AddTimezoneInfo(const std::string& city, int diff){
	 timezone_[city]=diff;

 }


  // 잘못된 값 입력시 false 리턴하고 원래 시간은 바뀌지 않음.

  bool SetTimezone(const std::string& timezone){
	  hour_-=time_difference_;
	  time_difference_=timezone_[timezone];
	
	  hour_+=timezone_[timezone];
	  if(hour_<0){hour_+=24;}
	  if(hour_>=24){hour_-=24;}
	  return true; 
  }


  int hour() const{return hour_;}

  int minute() const{return minute_;}

  int second() const{return second_;}

  int time_difference() const { return time_difference_; }


 private:

  // 여기에 시각 관련 멤버 변수 정의. (GMT 시간을 사용)
  int second_,minute_,hour_;


  // Timezone 관련 정보는 아래 변수에 저장. 시차는 시간 단위로만 계산.

  int time_difference_;

  static std::map<std::string, int> timezone_;

};


struct InvalidTimeException {

  std::string input_time;

  InvalidTimeException(const std::string& str) : input_time(str) {}

};


// hh:mm:ss 형식으로 입출력. 표준시가 아닌 경우 (+xx)/(-xx) 형식으로 시차를 표시.

std::ostream& operator<<(std::ostream& os, const WorldClock& c);


// hh:mm:ss 로 입력받음.

// 사용자 입력 오류시 >> operator는 InvalidDateException을 발생할 수 있음.

std::istream& operator>>(std::istream& is, WorldClock& c);

#endif

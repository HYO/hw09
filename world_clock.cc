#include "world_clock.h"
#include<string>
#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<map>
using namespace std; 

map<string,int> WorldClock::timezone_;

ostream& operator<<(ostream& os, const WorldClock& c){
	os<<c.hour()<<":"<<c.minute()<<":"<<c.second();
	if(c.time_difference()!=0)os<<" (+"<<c.time_difference()<<")";
	return os;
}

istream& operator>>(istream& is, WorldClock& c){
	char tmp1,tmp2;
	string str="";
	int hour,min,sec;
	is>>str;
	if(str.find(":")==2) {
  	  hour=atoi(str.substr(0,2).c_str());
  	  min=atoi(str.substr(3,2).c_str());
	  sec=atoi(str.substr(6,2).c_str());
	}
	if(str.find(":")==1) {
  	  hour=atoi(str.substr(0,1).c_str());
  	  min=atoi(str.substr(2,2).c_str());
	  sec=atoi(str.substr(5,2).c_str());
	}
	//is>>hour>>tmp1>>min>>tmp2>>sec;
	if(hour<0||hour>=24||min<0||min>=60||sec<0||sec>=60) {
		throw InvalidTimeException(str);
	}
	c.SetTime(hour,min,sec);
	return is;
}
